FROM php:7.4.10-apache

MAINTAINER ICEcoder <stefano.zizza@bertschi.com>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y git 
RUN a2enmod rewrite
RUN a2enmod ssl
RUN service apache2 restart

ENTRYPOINT if [ ! -d "/var/www/html/icecoder/" ]; then git clone https://github.com/icecoder/ICEcoder.git /var/www/html/icecoder/; fi && \
if [ ! -d "/var/www/html/schnuppertage/" ]; then git clone https://bitbucket.org/bertschiagch/schnuppertage_informatiker.git /var/www/html/schnuppertage; fi && \
chmod 755 /var/www/html && \
chown -R www-data.www-data /var/www/html && \
apache2-foreground
