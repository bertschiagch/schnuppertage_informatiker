# About
Dieses Repository wird von Bertschi AG zu Schulungszwecken für Schnupperlehringe verwendet und eingesetzt. Inhalte dieses Repositories werden für interne Zwecke verwendet und deshalb nur durch Bertschi AG Dürrenäsch verwaltet.
Die Daten dürfen jedoch auch nach einem Schnuppertag weiterhin verwendet werden!

# Inhalt

# HTML
* HTML_Aufgaben
    * aufgabe1
        * aufgabe1.html
    * aufgabe2
        * aufgabe2.html
        * bertschilogo.jpg
    * aufgabe3
        * aufgabe3.html
    * aufgabe4
        * aufgabe4.html
    * aufgabe5
        * aufgabe5.html

# PHP 
* PHP_Aufgaben
    * aufgabe1
        * aufgabe1.php
    * aufgabe2
        * server.php
        * style.css
    * aufgabe3
        * aufgabe3.php
        * style.css
    * aufgabe4
        * aufgabe4.php
        * style.css
* PHP_Tests
    * test.php



# Einrichtung der Testumgebung

Voraussetzungen:
* Chrome Browser
* Docker Desktop for Windows
* docker-compose

1. docker-compose up -d ausführen
2. Zugriff auf die Daten erfolgt über den Browser (zB. Internet Explorer) über http://localhost:8888/icecoder
3. Done!

## Dokumentationen und Web-Links
* http://www.php.net <- Komplette Dokumentation zu PHP

# Lizenz

[Creative Commons Lizenzvertrag](http://creativecommons.org/licenses/by-nc-nd/4.0/)
Schnuppertage DEMO Projekt von Bertschi AG ist lizenziert unter einer [Creative Commons Namensnennung - Nicht kommerziell - Keine Bearbeitungen 4.0 International Lizenz](http://creativecommons.org/licenses/by-nc-nd/4.0/)